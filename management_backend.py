import logging
import sys
import datetime
from pathlib import Path
import json
import argparse
import pandas as pd
import math
import csv
import ast
from collections import Counter
from flask import Flask, request, jsonify
from flask_cors import CORS,cross_origin
from flask_socketio import SocketIO, emit

##################
# logging format #
##################

log_format = '%(asctime)s - %(levelname)s - %(lineno)d - %(message)s'
current_time = datetime.datetime.utcnow().strftime("%Y_%m_%d")

# Set up a handler for standard logs
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.INFO)
stdout_handler.setFormatter(logging.Formatter(log_format))

# Set up a handler for error logs
stderr_handler = logging.StreamHandler(sys.stderr)
stderr_handler.setLevel(logging.ERROR)
stderr_handler.setFormatter(logging.Formatter(log_format))

logging.basicConfig(
    handlers=[stdout_handler, stderr_handler], 
    level=logging.DEBUG, 
    format =log_format, 
    datefmt="%m/%d/%Y %I:%M:%S %p %Z")

## set level of http transactions to log only warnings
log = logging.getLogger('werkzeug')
log.setLevel(logging.WARNING)

####################
# flask and socket #
####################

# set up flask
app = Flask(__name__)
CORS(app)

# set up socket
socketio = SocketIO(app, cors_allowed_origins = "*")
socketio.debug = False  # Enable debug mode

##################
# PATH FUNCTIONS #
##################
def GET_data_path(chain_name, csv_name):
    '''
    Returns path depending on specified store chain and csv
    Args:
        chain_name (str): name of the store chain (i.e. coco)
        csv_name (str): name of csv (i.e. stock.csv)
    '''
    directory = Path(__file__).resolve().parents[1].joinpath("data", chain_name)
    sel_csv = directory.joinpath(csv_name)
    return sel_csv

def GET_chain_store_data(chain_name, store_id, csv_name):
    '''
    Returns path depending on specified store chain and csv
    Args:
        chain_name (str): name of the store chain (i.e. coco)
        store_id (str): name of store id (i.e. 1001)
        csv_name (str): name of csv (i.e. menu.csv)
    '''
    directory = Path(__file__).resolve().parents[1].joinpath("data", chain_name, "management", store_id)
    csv_path = directory.joinpath(csv_name)
    menu_csv = pd.read_csv(csv_path, encoding = "utf-8-sig")
    return menu_csv


####################
# GLOBAL VARIABLES #
####################

# define path
data_dir = Path(__file__).resolve().parents[1].joinpath("data", "dumpling")
store_config = json.load(open(data_dir.joinpath("store_config.json")))
stock_df_path = data_dir.joinpath("stock.csv")
lock_df_path = data_dir.joinpath("kiosk_lock.csv")

# flag for stock update 
kiosk_stock_update = {}
not_updated_stock = {}

# flag for kiosk lock update
kiosk_lock_update = {}
not_updated_lock = {}

# formatting flags
for store, detail in store_config[store_config["MODE"]].items():
    # stock update flag
    kiosk_stock_update[store] = detail["DEVICE_LIST"]
    not_updated_stock[store] = []
    # kiosk lock update flag
    kiosk_lock_update[store] = detail["DEVICE_LIST"]
    not_updated_lock[store] = []

logging.critical(f"kiosk_stock_update: {kiosk_stock_update}")
logging.critical(f"not_updated_stock: {not_updated_stock}")
logging.critical(f"kiosk_lock_update: {kiosk_lock_update}")
logging.critical(f"not_updated_lock: {not_updated_lock}")

##########

menu_df = pd.read_csv(data_dir.joinpath("menu.csv"))
printer_json = json.load(open(data_dir.joinpath("printer_format.json")))
# flag for kitchen printer
kitchen_print_all = False # default is False
# flag for kitchen printer switch
printer_switch = False # default is False
prev_printer = ""

####################
# STOCK MANAGEMENT #
####################
# API for coco kiosks to download stock data from the server
@app.route("/management/kiosks_get_stock", methods = ["GET"])
@cross_origin(supports_credentials = True)
def kiosks_get_stock():
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "dev", type = str)
        chain = request.args.get("store_chain", default = "dumpling", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/kiosks_get_stock'. Please check backend log."
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # generating stock data in that store for manager portal
    csv_path = GET_data_path(chain, "stock.csv")
    stock_df = csv.DictReader(open(csv_path, encoding = "utf-8-sig"))
    response_dict = {}
    for row in stock_df:
        
        # only check items that are on menu
        if row["On_Menu"].title() == "False":
           continue

        # key item ID, value stock bool
        response_dict[row["Item ID"]] = ast.literal_eval(row[store_id].title())
        
    logging.info(f"Sending stock data to {chain} store {store_id}: {response_dict}")
    return jsonify(response_dict), 200
    

# API for online ordering to call to download stock data from server
@app.route("/management/stock_online", methods = ["GET"])
@cross_origin(supports_credentials = True)
def online_get_stock():
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "dev", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/stock_online'. Please check backend log."
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # generating stock data in that store for manager portal
    stock_df = csv.DictReader(open(stock_df_path, encoding = "utf-8-sig"))
    response_dict = {}
    for row in stock_df:

        # only check items that are on menu
        if row["On_Menu"].title() == "False":
           continue

        # key item ID, value stock bool
        response_dict[row["Item ID"]] = ast.literal_eval(row[store_id].title())
        
    logging.info(f"ONLINE: sending stock data to {store_id} manager portal: {response_dict}")
    return jsonify(response_dict), 200

# API for manager portal frontend to call to download stock data from server
@app.route("/management/portal_download_stock", methods = ["GET"])
@cross_origin(supports_credentials = True)
def portal_download_stock():
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "dev", type = str)
        chain = request.args.get("store_chain", default = "dumpling", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/portal_download_stock'. Please check backend log."
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # generating stock data in that store for manager portal
    csv_path = GET_data_path(chain, "stock.csv")
    stock_df = csv.DictReader(open(csv_path, encoding = "utf-8-sig"))
    
    try:
        # get menu csv for store
        menu_df = GET_chain_store_data(chain, store_id, "menu.csv")
        menu_df = menu_df[menu_df["on_menu"].isin(["TRUE", True])]
        id_text = "id"
        items_list = menu_df[id_text].tolist()
        
        # get modifiers
        mod_df = GET_chain_store_data(chain, store_id, "modifier_keys.csv")
        
        # skip specific modifier keys for each chain store
        if chain in ["dumpling"]:
            mod_df = mod_df[~mod_df["modifier_group"].isin(["Size", "Combo"])]
        
        mod_list = mod_df["id"].tolist()
        items_list.extend(mod_list)

    except Exception as error:
        message = "Data error on server 'management/portal_download_stock'. Please check backend log."
        logging.exception(f"{message} due to {error}")
        return jsonify(message = message), 400

    response_dict = {}
    for row in stock_df:
        # skip items that are not on the menu for the store
        if row["Item ID"] not in items_list:
            continue
        # only check items that are on menu
        if row["On_Menu"].title() == "False":
           continue

        # extract data
        info_dict = {
            "Item En": row["Sub_English_Name"],
            "Item Zh": row["Sub_Chinese_Name"],
            "Stock": ast.literal_eval(row[store_id].title())
        }
        response_dict[row["Item ID"]] = info_dict
        
    logging.info(f"Sending update stock data to {chain} {store_id} manager portal: {response_dict}")
    return jsonify(response_dict), 200

# API for manager portal frontend to call for notifying and sending stock updates to the server
@app.route("/management/portal_update_stock", methods = ["GET"])
@cross_origin(supports_credentials = True)
def portal_update_stock():
    global kiosk_stock_update
    global not_updated_stock
    
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "dev", type = str)
        chain = request.args.get("store_chain", default = "dumpling", type = str)
        item_id = request.args.get("item_id", default = "A1", type = str)
        in_stock = request.args.get("in_stock", default = "True", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/portal_update_stock'. Please check backend log"
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # validating parameters (store_id, item_id)
    csv_path = GET_data_path(chain, "stock.csv")
    stock_df = pd.read_csv(csv_path)

    items = list(stock_df["Item ID"])
    stores = list(stock_df.columns.drop(["Item ID", "Sub_English_Name", "Sub_Chinese_Name", "On_Menu"]))

    if item_id not in items:
        message = "Invalid Item ID from the request 'management/portal_update_stock'."
        logging.error(f"{message}")
        return jsonify(message = message), 400

    if store_id not in stores:
        message = "Invalid store number from the request 'management/portal_update_stock'."
        logging.error(f"{message}")
        return jsonify(message = message), 400

    try:
        # updating source stock data
        stock_df.loc[(stock_df["Item ID"] == item_id), store_id] = in_stock.title()
        stock_df.to_csv(csv_path, index = False)
        
        if chain == "dumpling":
            # resetting stock update and not updated tracking list
            kiosk_stock_update[store_id] = []
            not_updated_stock[store_id] = []

        logging.info(f"Stock update from client {chain} {store_id}, changing {item_id} to {in_stock}.")
        return jsonify(message = "Stock updated successfully!"), 200

    except Exception as error:
        message = "Error in saving stock data at 'management/portal_update_stock'"
        logging.error(f"{message} due to: {error}")
        return jsonify(message = message), 400

# API for local devices to call for downloading stock data from the server
@app.route("/management/kiosk_download_stock", methods = ["GET"])
@cross_origin(supports_credentials=True)
def kiosk_download_stock():
    global kiosk_stock_update

    try:
        # get parameter from API request
        store_id = request.args.get("store_id", default = "dev", type = str)
        kiosk_name = request.args.get("kiosk_name", default = "", type = str)
        
        # if that device has already finished update -> return (1) status code 204 and (2) message for no updates
        if kiosk_name in kiosk_stock_update[store_id]:
            message = "No stock update!"
            return jsonify(message = message), 204
        
        # if kiosk has not updated yet -> return (1) status code 200 and (2) updated stock data
        elif kiosk_name not in kiosk_stock_update[store_id]:
            stock_json = pd.read_csv(stock_df_path).to_json(orient = "records", force_ascii = False)
            logging.info(f"Sending stock.csv to {store_id}, {kiosk_name}")
            message = {"message": "Sending stock.csv", "stock_status": stock_json}
            return jsonify(message = message), 200        
            
    except Exception as error:
        message = "Error sending stock at 'management/kiosk_download_stock'"
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

# get stock update response from kiosks
@app.route("/management/kiosk_stock_response", methods = ["POST"])
@cross_origin(supports_credentials = True)
def kiosk_stock_response():
    global kiosk_stock_update
    global not_updated_stock

    try:
        kiosk_response = request.get_json()
        store_id = kiosk_response["store_name"]
        kiosk_name = kiosk_response["kiosk_name"]
        logging.info(f"FROM CLIENT for '/management/kiosk_stock_response': {kiosk_response}")

        # push updated store name and kiosk name to the list
        if kiosk_response["status"] == "updated":
            kiosk_stock_update[store_id].append(kiosk_name)
            logging.info(f"Update stock status for store {store_id}: {kiosk_stock_update[store_id]}")
        
        else:
            logging.error(f"Invalid kiosk response at 'management/kiosk_stock_response': {kiosk_response}")
            message = {"message": "Invalid kiosk response at 'management/kiosk_stock_response'", "kiosk_stock_update": kiosk_stock_update, "kiosk_stock_not_updated": not_updated_stock}
            return jsonify(message = message), 400
        
        try:
            # log warning if other kiosks that are not yet updated
            kiosk_list = set(store_config[store_config["MODE"]][store_id]["DEVICE_LIST"])
            updated_list = set(kiosk_stock_update[store_id])
            not_updated_list = sorted(kiosk_list.difference(updated_list))
            
            if len(not_updated_list) == 0:
                logging.info("All kiosks stock updated.")
            else:
                logging.warning(f"Stock not updated for store {store_id}: {not_updated_list}")
            
            # log error if other kiosks has not been updated since the second round
            not_updated_stock[store_id] += not_updated_list
            not_updated_count = Counter(not_updated_stock[store_id])
            kiosk_stock_error = list([kiosk for kiosk in not_updated_count if not_updated_count[kiosk] > (len(kiosk_list) - 1)])
            if len(kiosk_stock_error) > 0:
                logging.error(f"Stock not updated for store {store_id}: {kiosk_stock_error}")
            
            message = {"message": "Received kiosk stock update response", "kiosk_stock_update": kiosk_stock_update, "kiosk_stock_not_updated": not_updated_stock}
            return jsonify(message = message), 200
        
        except Exception as error:
            logging.error(f"Error in tracking kiosks that has not updated stock: {error}")
            message = {"message": "Error in tracking kiosks that has not updated stock", "kiosk_stock_update": kiosk_stock_update, "kiosk_stock_not_updated": not_updated_stock}
            return jsonify(message = message), 400
    
    except Exception as error:
        logging.error(f"Invalid request at 'management/kiosk_stock_response': {error}")
        message = {"message": "Invalid request at 'management/kiosk_stock_response'","kiosk_stock_update": kiosk_stock_update, "kiosk_stock_not_updated": not_updated_stock}
        return jsonify(message = message), 400

#########################
# KIOSK LOCK MANAGEMENT #
#########################
# API for kiosks to download lock data from the server
@app.route("/management/lock_kiosks", methods = ["GET"])
@cross_origin(supports_credentials = True)
def get_lock_kiosks():
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "dev", type = str)
        kiosk_name = request.args.get("kiosk_name", default = "kiosk1", type = str)
        chain = request.args.get("store_chain", default = "dumpling", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/lock_kiosks'. Please check backend log."
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # generating stock data in that store for manager portal
    csv_path = GET_data_path(chain, "kiosk_lock.csv")
    lock_df = pd.read_csv(csv_path)
    
    try:
        # get the lock status based on api parameters
        row_index = lock_df.index[lock_df["Kiosk ID"] == kiosk_name].tolist()[0]
        lock_status = lock_df.loc[row_index, store_id]

        if type(lock_status) == bool:
            response_dict = {"LOCK": lock_status}
            # logging.info(f"COCO KIOSKS: sending lock data to coco store {store_id}: {response_dict}")
            return jsonify(response_dict), 200
        else:
            message = f"Lock status is not a boolean check data at 'management/lock_kiosks'. Please check backend log."
            logging.error(f"{message}")
            return jsonify(message = message), 400
        
    except Exception as error:
        message = f"Client params: {chain}, {store_id}, {kiosk_name} at 'management/lock_kiosks'. Please check backend log."
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400


# API for manager portal frontend to call to download kiosks lock data from server
@app.route("/management/portal_download_lock", methods = ["GET"])
@cross_origin(supports_credentials = True)
def portal_download_lock():
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "1001", type = str)
        chain = request.args.get("store_chain", default = "dumpling", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/kiosk_lock'. Please check backend log."
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # generating lock data in that store for manager portal
    csv_path = GET_data_path(chain, "kiosk_lock.csv")
    lock_df = csv.DictReader(open(csv_path, encoding = "utf-8-sig"))
    response_dict = {}
    for row in lock_df:
        if row[store_id]:
            response_dict[row["Kiosk ID"]] = {
                "Name": row["Name"],
                "Locked": ast.literal_eval(row[store_id].title())
            }
    
    logging.info(f"Sending kiosk lock data to {chain} store {store_id} manager portal: {response_dict}")
    return jsonify(response_dict), 200

# API for manager portal frontend to call for notifying and sending kiosk lock updates to the server
@app.route("/management/portal_update_lock", methods = ["GET"])
@cross_origin(supports_credentials = True)
def portal_update_lock():
    global kiosk_lock_update
    global not_updated_lock
    
    # get parameters from the API request
    try:
        store_id = request.args.get("store_number", default = "dev", type = str)
        chain = request.args.get("store_chain", default = "dumpling", type = str)
        kiosk_id = request.args.get("kiosk_id", default = "kiosk1", type = str)
        lock_status = request.args.get("locked", default = "False", type = str)
    except Exception as error:
        message = "Missing required parameters from the request 'management/portal_update_lock'. Please check backend log"
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

    # validating parameters (store_id, kiosk_id)
    csv_path = GET_data_path(chain, "kiosk_lock.csv")
    lock_df = pd.read_csv(csv_path)
    stores = list(lock_df.columns.drop(["Kiosk ID", "Name"]))
    if store_id not in stores:
        message = "Invalid store number from the request 'management/portal_update_lock'."
        logging.error(f"{message}")
        return jsonify(message = message), 400
    
    kiosks = list(lock_df.loc[pd.notnull(lock_df[store_id]), "Kiosk ID"])
    if kiosk_id not in kiosks:
        message = "Invalid Kiosk ID from the request 'management/portal_update_lock'."
        logging.error(f"{message}")
        return jsonify(message = message), 400

    try:
        # updating source lock data
        lock_df.loc[(lock_df["Kiosk ID"] == kiosk_id, store_id)] = lock_status.title()
        lock_df.to_csv(csv_path, index = False)

        if chain == "dumpling":
            # resetting lock update and not updated tracking list
            try:
                kiosk_lock_update[store_id].remove(kiosk_id)
                not_updated_lock[store_id] = [kiosk for kiosk in not_updated_lock[store_id] if kiosk != kiosk_id] 
            except ValueError as Exception:
                logging.info(f"{kiosk_id} is still updating.")

        logging.info(f"Lock update from client {chain} {store_id}, changing {kiosk_id} to {lock_status}.")
        return jsonify(message = "Kiosk lock updated successfully!"), 200

    except Exception as error:
        message = "Error in saving lock data at 'management/portal_update_lock'"
        logging.error(f"{message} due to: {error}")
        return jsonify(message = message), 400

# API for local devices to call for downloading lock data from the server
@app.route("/management/kiosk_download_lock", methods = ["GET"])
@cross_origin(supports_credentials = True)
def kiosk_download_lock():
    global kiosk_lock_update

    try:
        # get parameter from API request
        store_id = request.args.get("store_id", default = "dev", type = str)
        kiosk_name = request.args.get("kiosk_name", default = "kiosk1", type = str)
    
        # if that device has already finished update -> return (1) status code 204 and (2) message for no updates
        if kiosk_name in kiosk_lock_update[store_id]:
            message = "No lock update!"
            return jsonify(message = message), 204
        
        # if kiosk has not updated yet -> return (1) status code 200 and (2) updated lock data
        elif kiosk_name not in kiosk_lock_update[store_id]:
            lock_df = csv.DictReader(open(lock_df_path, encoding = "utf-8-sig"))
            lock_json = {}
            for row in lock_df:
                if row[store_id]:
                    lock_json[row["Kiosk ID"]] = row[store_id].title()
            logging.info(f"Sending lock_config.json to {store_id}, {kiosk_name}")
            message = {"message": "Sending lock_config.json", "lock_status": lock_json}
            return jsonify(message = message), 200
    
    except Exception as error:
        message = "Error in sending lock data at 'management/kiosk_download_lock'"
        logging.error(f"{message} due to {error}")
        return jsonify(message = message), 400

# get response from kiosks about lock status
@app.route("/management/kiosk_lock_response", methods = ["POST"])
@cross_origin(supports_credentials = True)
def kiosk_lock_response():
    global kiosk_lock_update
    global not_updated_lock

    try:
        kiosk_response = request.get_json()
        store_id = kiosk_response["store_name"]
        kiosk_name = kiosk_response["kiosk_name"]
        logging.info(f"FROM CLIENT for 'management/kiosk_lock_response': {kiosk_response}")
        
        # push updated store name and kiosk name to the list
        if kiosk_response["status"] == "updated":
            kiosk_lock_update[store_id].append(kiosk_name)
            logging.info(f"Update lock status for store {store_id}: {kiosk_lock_update[store_id]}")
        
        else:
            logging.error(f"Invalid kiosk response at 'management/kiosk_lock_response': {kiosk_response}")
            message = {"message": "Invalid kiosk response at 'management/kiosk_lock_response'", "kiosk_lock_update": kiosk_lock_update, "kiosk_lock_not_updated": not_updated_lock}
            return jsonify(message = message), 400
        
        try:
            # log warning if other kiosks that are not yet updated
            kiosk_list = set(store_config[store_config["MODE"]][store_id]["DEVICE_LIST"])
            updated_list = set(kiosk_lock_update[store_id])
            not_updated_list = sorted(kiosk_list.difference(updated_list))
            
            if len(not_updated_list) == 0:
                logging.info("All kiosks lock updated.")
            else:
                logging.warning(f"Lock not updated for store {store_id}: {not_updated_list}")
            
            # log error if other kiosks has not been updated since the second round
            not_updated_lock[store_id] += not_updated_list
            not_updated_count = Counter(not_updated_lock[store_id])
            kiosk_lock_error = list([kiosk for kiosk in not_updated_count if not_updated_count[kiosk] > (len(kiosk_list) - 1)])
            if len(kiosk_lock_error) > 0:
                logging.error(f"Lock not updated for store {store_id}: {kiosk_lock_error}")
            
            message = {"message": "Received kiosk lock update response", "kiosk_lock_update": kiosk_lock_update, "kiosk_lock_not_updated": not_updated_lock}
            return jsonify(message = message), 200
        
        except Exception as error:
            logging.error(f"Error in tracking kiosks that has not updated lock: {error}")
            message = {"message": "Error in tracking kiosks that has not updated lock", "kiosk_lock_update": kiosk_lock_update, "kiosk_lock_not_updated": not_updated_lock}
            return jsonify(message = message), 400
    
    except Exception as error:
        logging.error(f"Invalid request at 'management/kiosk_lock_response': {error}")
        message = {"message": "Invalid request at 'management/kiosk_lock_response'","kiosk_lock_update": kiosk_lock_update, "kiosk_lock_not_updated": not_updated_lock}
        return jsonify(message = message), 400
  
############ ABOVE FUNCTIONS ARE TESTED AND ARE DEPLOYING ###########

# api to GET all menu items
@app.route("/management/full_menu", methods=["GET"])
@cross_origin(supports_credentials=True)
def full_menu():
    menu_json = {}
    columns = menu_df.columns[1:]  # ignore ID column
    try:
        for index, row in menu_df.iterrows():
            item_id = row["ID"]
            menu_json[item_id] = {}
            for column in columns:
                try:
                    if math.isnan(row[column]):
                        menu_json[item_id][column] = ""
                    else:
                        menu_json[item_id][column] = row[column]
                except:
                    menu_json[item_id][column] = row[column]
        logging.info("Menu parsed successfully.")

    except Exception as e:
        logging.warning("Error parsing menu at /management/full_menu")
        return "Error parsing menu.", 400

    logging.info("Sent menu data to client")
    # logging.info("DEBUG: {}".format(menu_json))
    return jsonify(menu_json), 200

###################
# KITCHEN PRINTER #
###################
# api to TROUBLESHOOT kitchen printer (print all queued orders)
@app.route("/management/kitchen_printer/print_all", methods=["GET"])
@cross_origin(supports_credentials=True)
def req_print_all():
    global kitchen_print_all
    try:
        printer_name = request.args.get("printer_name", default="front_kitchen", type=str)
    except:
        logging.error("Invalid request at /management/kitchen_printer/print_all")
        return "Invalid request.", 400
    
    try:
        # change flag to True
        kitchen_print_all = True
        logging.info("Request to print all queued orders for {}".format(printer_name))
        return jsonify(message="Request print all"), 200
    except Exception as e:
        s = "Error requesting print all"
        logging.error(s + " at /management/kitchen_printer/print_all")
        return jsonify(message=s), 400


# send print all request to minipc (kitchen printer) IF kitchen_print_all is True
@app.route("/management/kitchen_printer/send_print_all")
@cross_origin(supports_credentials=True)
def send_print_all():
    global kitchen_print_all
    try:
        printer_name = request.args.get("printer_name", default="front_kitchen", type=str)
    except:
        return jsonify(message=s), 400  

    try:
        # ONLY send request to print all queued orders IF kitchen_print_all is True
        if kitchen_print_all == True:
            logging.info("Sending request to print all queued orders for {}".format(printer_name))
            return jsonify(message="Print All"), 200
        elif kitchen_print_all == False:
            return jsonify(message="No print update!"), 204
            
    except Exception as e:
        s = "Error sending print update."
        logging.error(s + " at /management/kitchen_printer/send_print_all")
        return jsonify(message=s), 400
    

# get response from minipc (kitchen printer)
@app.route("/management/kitchen_printer/res_print_all", methods=["POST"])
@cross_origin(supports_credentials=True)
def res_print_all():
    global kitchen_print_all

    try:
        data = request.get_json()
        name = data["name"]
        logging.info("FROM CLIENT: {}".format(data))
        if data["status"] == "printed":
            # reset the flag when minipc has updated data
            kitchen_print_all = False
            logging.info("{} PROCESSED PRINT ALL REQUEST".format(name))
            return 'Received data acknowledgment processed.', 200
        else:
            logging.error("Invalid request at /management/kitchen_printer/res_print_all")
            return 'Invalid request.', 400
    except Exception as e:
        s = "Exception "
        logging.warning(s + e + " at /management/kitchen_printer/res_print_all")
        return jsonify(message=s), 400


##########################
# SWITCH KITCHEN PRINTER #
##########################
# api to GET printer data 
@app.route("/management/kitchen_printer/printer_data")
@cross_origin(supports_credentials=True)
def send_printer_json():
    global printer_json
    try:
        logging.info("Sent printer data to client")      
        return jsonify(printer_json), 200
    except Exception as e:
        s = "Error sending printer_format.json"
        logging.error(s + " at /management/kitchen_printer/printer_data")
        return jsonify(message=s), 400

# update PRINTER_NAME in printer_format.json
@app.route("/management/kitchen_printer/switch_printer")
@cross_origin(supports_credentials=True)
def switch_printer():
    global data_dir
    global printer_json
    global prev_printer
    global printer_switch
    try:
        new_printer = request.args.get("printer_name", default="front_kitchen", type=str)
    except:
        return jsonify(message=s), 400

    # Updating
    try:
        # save previous printer name and then update new printer
        prev_printer = printer_json["PRINTER_NAME"]
        printer_json["PRINTER_NAME"] = new_printer
        # Write the dictionary to the JSON file
        with open(f"{data_dir}/dumpling/printer_format.json", 'w') as f:
            json.dump(printer_json, f, indent=4)
            
        # ping minipc when there is an update and send new print_format.json
        printer_switch = True
        logging.info("There was printer switch request from stock management client to " + new_printer + " from " + prev_printer)

    except Exception as e:
        s = "Error with printer switch request."
        logging.error(s + " at /management/kitchen_printer/switch_printer")
        return jsonify(message=s), 400

    return jsonify(message="Printer switch successful!"), 200


# send switch printer to minipc (new printer_format.json) IF printer_switch is True
@app.route("/management/kitchen_printer/send_switch_printer")
@cross_origin(supports_credentials=True)
def send_switch_printer():
    global printer_json
    global printer_switch
    global prev_printer

    try:
        # ONLY send printer_format.json IF printer_switch is True
        if printer_switch == True:
            logging.info("Sending request to switch to {} printer".format(printer_json["PRINTER_NAME"]))
            # send printer_format.json and previous printer name
            send_data = {"json": printer_json, "old_printer": prev_printer}
            return jsonify(send_data), 200
        elif printer_switch == False:
            return jsonify(message="No print switch!"), 204
            
    except Exception as e:
        s = "Error sending printer_format.json for printer switch."
        logging.error(s + " at /management/kitchen_printer/send_switch_printer")
        return jsonify(message=s), 400


# get response from minipc (kitchen printer)
@app.route("/management/kitchen_printer/res_switch_printer", methods=["POST"])
@cross_origin(supports_credentials=True)
def res_switch_printer():
    global printer_json
    global printer_switch

    try:
        data = request.get_json()
        name = data["name"]
        logging.info("FROM CLIENT: {}".format(data))
        if data["status"] == "switched":
            # reset the flag when minipc has updated data
            printer_switch = False
            logging.info("{} PROCESSED PRINTER SWITCH REQUEST".format(name))
            return 'Received data acknowledgment processed.', 200
        else:
            logging.error("Invalid request at /management/kitchen_printer/res_switch_printer")
            return 'Invalid request.', 400
    except Exception as e:
        s = "Exception "
        logging.warning(s + e + " at /management/kitchen_printer/res_switch_printer")
        return jsonify(message=s), 400


##########

################################
# CHECK BACKEND UP AND RUNNING #
################################
@app.route("/management", methods=["GET"])
@cross_origin(supports_credentials=True)
def hello():
    return json.dumps({"status": "Management Portal Backend Running!"})


####################
# SOCKET FUNCTIONS #
####################
# # create socket functions
@socketio.on('connect')
def connect():
    logging.info('Client CONNECTED from socket')

@socketio.on('disconnect')
def disconnect():
    logging.info('Client with DISCONNECTED from socket')

# @socketio.on('send_stock')
# def send_stock_csv(data):
#     # Broadcast the CSV data to connected clients
#     socketio.emit('receive_csv', data)
#     print("SENT STOCK CSV")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--port", type=int, default=9000, help="Port number")
    args = parser.parse_args()

    # # Run the Flask app on the specified port
    # app.run(host="0.0.0.0", port=args.port, debug=True)
    socketio.run(app, host='0.0.0.0', port=args.port, debug=True)