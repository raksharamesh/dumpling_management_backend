import pandas as pd
from pathlib import Path

#####################
# HELPER FUNCTIONS #
####################
def csv_in(data_path, csv_name):
    '''
    Returns pandas df from csv

    Args:
        data_path: path to data folder
        csv_name: name of csv file
    '''
    csv_path = Path(data_path).joinpath(csv_name)
    df = pd.read_csv(csv_path)
    return df

def csv_out(data_path, csv_name, data):
    '''
    Writes a csv file using defined data and saves it according to the json_path and file_name

    Args:
        data_path (str): path to desired directory 
        file_name (str): name of the csv
        data (pands dataframe): dictionary to populate the csv
    '''
    output_path = Path(data_path).joinpath(csv_name)
    data.to_csv(output_path, encoding="utf-8-sig", index=False)

def GET_chain_store_data(chain_name, store_id, csv_name):
    '''
    Returns path depending on specified store chain and csv
    Args:
        chain_name (str): name of the store chain (i.e. coco)
        store_id (str): name of store id (i.e. 1001)
        csv_name (str): name of csv (i.e. menu.csv)
    '''
    directory = Path(__file__).resolve().parents[1].joinpath("data", chain_name, "management", store_id)
    csv_path = directory.joinpath(csv_name)
    df = pd.read_csv(csv_path, encoding = "utf-8-sig")
    return df


def helper_extract_colID(col_list):
    '''
    Returns cleaned list with just the modifier ids
    Args:
        col_list (list): list of values from columns of a modifier df
    '''
    col_list_c = [term for term in col_list if term != "id" and not term.startswith("price_")]
    return col_list_c


def helper_coco_stock(df, dictionary, keys):
    '''
    Returns a dictionary with stock info

    df (pandas dataframe): dataframe to iterate through
    dictionary (dictionary): dictionary to push data into
    keys (list): dictionary keys
    '''    
    for i in range(len(df)):
        id = df.at[i, "id"]
        for key in keys:
            try:
                if key == "english_name":
                    # remove id from english name
                    if id in df.at[i, key]:
                        new_value = df.at[i, key].replace(id + ". ", "")
                        dictionary[key].append(new_value)
                    else:
                        dictionary[key].append(df.at[i, key])
                else:
                    dictionary[key].append(df.at[i, key])
            except:
                # store id columns
                menu = GET_chain_store_data("coco", key, "menu.csv")
                mod_milk = GET_chain_store_data("coco", key, "modifier_milk.csv")
                mod_toppings = GET_chain_store_data("coco", key, "modifier_toppings.csv")
                # get list of all exisiting items in a specific store
                items_list = menu["id"].tolist()
                mod_milk_list = helper_extract_colID(mod_milk.columns.tolist())
                mod_toppings_list = helper_extract_colID(mod_toppings.columns.tolist())
                # create key value pair
                menu_dict = dict(zip(menu["id"], menu["on_menu"]))

                # key exists as a col in df then append true (in stock)
                if id in items_list:
                    dictionary[key].append(bool(menu_dict[id]))
                elif id in mod_milk_list:
                    dictionary[key].append(True)
                elif id in mod_toppings_list:
                    dictionary[key].append(True)
                else:
                    dictionary[key].append(False)
    
    return dictionary

def helper_dumpling_stock(df, dictionary, keys):
    '''
    Returns a dictionary with stock info

    df (pandas dataframe): dataframe to iterate through
    dictionary (dictionary): dictionary to push data into
    keys (list): dictionary keys
    '''
    for i in range(len(df)):
        id = df.at[i, "ID"]
        for key in keys:
            try:
                dictionary[key].append(df.at[i, key])
            except:
                # store id columns
                menu = GET_chain_store_data("dumpling", key, "menu.csv")
                mod_soup = GET_chain_store_data("dumpling", key, "modifier_soupbase.csv")
                # get list of all exisiting items in a specific store
                items_list = menu["ID"].tolist()
                mod_soup_list = mod_soup["ID"].tolist()
                # create key value pair
                menu_dict = dict(zip(menu["ID"], menu["On_Menu"]))
                mod_soup_dict = dict(zip(mod_soup["ID"], mod_soup["On_Menu"]))

                # key exists as a col in df then append true (in stock)
                if id in items_list:
                    dictionary[key].append(bool(menu_dict[id]))
                elif id in mod_soup_list:
                    dictionary[key].append(bool(mod_soup_dict[id]))
                else:
                    dictionary[key].append(False)
    
    return dictionary


###############
# CREATE CSV #
##############
def create_coco_stock_csv(data_path_in, data_path_out, store_list):
    '''
    Creates stock.csv

    Args:
        data_path_in: path to data folder
        data_path_out: path to data folder for stock.csv
        store_list: list of stores that should be added as columns to the stock.csv (i.e. ["dev", "1001"])
    '''
    menu = csv_in(data_path_in, "menu.csv")
    mods = csv_in(data_path_in, "modifier_keys.csv")
    # clean data
    menu.fillna("", inplace=True)
    mods.fillna("", inplace=True)

    # instantiate stock dictionary
    stock_dict = {
        "id": [],
        "english_name": [],
        "chinese_name": [],
        "on_menu": []
    }
    for item in store_list:
        stock_dict[item] = []
    # get keys from stock_dict
    stock_dict_keys = list(stock_dict.keys())

    # iterate through menu
    stock_dict = helper_coco_stock(menu, stock_dict, stock_dict_keys)

    # only read these modifier groups from mods
    mod_list = ["Toppings", "Milk"]
    mods_filtered = mods[mods.modifier_group.isin(mod_list)].reset_index(drop=True)
    # iterate through mods
    stock_dict = helper_coco_stock(mods_filtered, stock_dict, stock_dict_keys)

    # create dataframe
    stock_df = pd.DataFrame(stock_dict)
    # update col names to be consistent with dumpling 
    col_names = {"id": "Item ID", "english_name": "Sub_English_Name", "chinese_name": "Sub_Chinese_Name", "on_menu": "On_Menu"}
    stock_df.rename(columns=col_names, inplace=True)

    # read out as csv
    csv_out(data_path_out, "stock.csv", stock_df)
    print("created coco stock.csv")


def create_dumpling_stock_csv(data_path_in, data_path_out, store_list):
    '''
    Create stock.csv 

    Args:
        data_path_in: path to data folder
        data_path_out: path to data folder for stock.csv
        store_list: list of stores that should be added as columns to the stock.csv (i.e. ["dev", "1001"])
    '''
    # read in data
    menu = csv_in(data_path_in, "menu.csv")
    mod_soup = csv_in(data_path_in, "modifier_soupbase.csv")
    # clean data
    menu.fillna("", inplace=True)
    mod_soup.fillna("", inplace=True)

    # instantiate stock dictionary
    stock_dict = {
        "ID": [],
        "Sub_English_Name": [],
        "Sub_Chinese_Name": [],
        "On_Menu": []
    }
    for item in store_list:
        stock_dict[item] = []
    # get keys from stock_dict
    stock_dict_keys = list(stock_dict.keys())

    # iterate through menu
    stock_dict = helper_dumpling_stock(menu, stock_dict, stock_dict_keys)
    stock_dict = helper_dumpling_stock(mod_soup, stock_dict, stock_dict_keys)

    # create dataframe
    stock_df = pd.DataFrame(stock_dict)
    # update col names to be consistent with dumpling 
    stock_df.rename(columns={"ID": "Item ID"}, inplace=True)


    # read out stock_df to csv
    csv_out(data_path_out, "stock.csv", stock_df)
    print("created dumpling stock.csv")


def create_kiosk_lock_csv(chain_name, new_store_id, kiosk_list):
    '''
    Append a new store as a column in kiosk_lock.csv

    Args:
        chain_name (str): indicates which store chain
        new_store_id (str): indicates the new id of the store
        kiosk_list (list): indicate how many kiosks would exist 
    '''
    data_path = Path(__file__).resolve().parents[1].joinpath("data", chain_name)
    kiosk_lock = csv_in(data_path, "kiosk_lock.csv")
    kiosk_lock.fillna("", inplace=True)
    kiosk_lock_dict = dict(kiosk_lock)

    # get lenght of data for new list
    row_len = len(kiosk_lock)
    # turn kiosk_list into a list of booleans
    bool_list = [True for i in kiosk_list]
    while len(bool_list) < row_len:
        bool_list.append("")
    kiosk_lock_dict[new_store_id] = bool_list

    # convert back to df
    new_kiosk_lock = pd.DataFrame(kiosk_lock_dict)

    # read out csv
    csv_out(data_path, "kiosk_lock.csv", new_kiosk_lock)
    print(f"created new {chain_name} kiosk_lock.csv")
    



if __name__ == "__main__":
    path_coco = Path(__file__).resolve().parents[1].joinpath("data", "coco")
    path_dumpling = Path(__file__).resolve().parents[1].joinpath("data", "dumpling")

    create_coco_stock_csv(path_coco, path_coco, ["dev", "carts", "1001", "1005"])
    create_dumpling_stock_csv(path_dumpling, path_dumpling, ["dev", "1001", "1002", "1003"])
    # create_kiosk_lock_csv("coco", "testing", ["k1"])